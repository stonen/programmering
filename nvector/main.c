#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"

int main(){
	int n =4;
	nvector *v = nvector_alloc(n);     
	nvector_set(v, 1, 3);
	nvector_set(v, 2, 6);
	nvector_set(v, 3, 4);
	nvector *u = nvector_alloc(n);     
	nvector_set(u, 1, 3);
	nvector_set(u, 2, 1);
	nvector_set(u, 3, 2); 
	printf("Checking if everything is working by printing u\n");
	printf("u = (%g,%g,%g)\n",nvector_get(u,1),nvector_get(u,2),nvector_get(u,3)); 
	printf("Dot product of u and v should give 23\n");
	printf("using our function we get: %g\n",nvector_dot_product (u,v));          
	nvector_free(v);
	nvector_free(u);
}

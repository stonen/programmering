#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_odeiv2.h>
#include <assert.h>
#include <gsl/gsl_errno.h>

double Fe(double e,double r);

int master(const gsl_vector * x, void *params, gsl_vector * f){
	double rmax = *(double*)params;
	double e = gsl_vector_get (x, 0);
  	double y0 = Fe(e,rmax);
  	gsl_vector_set (f, 0, y0);
	return GSL_SUCCESS;
}

int main (){
  	const int dim = 1;
  	const double rmax = 8;
  	
	gsl_multiroot_function function = {&master, dim, (void*)&rmax};
  	gsl_vector * x = gsl_vector_alloc(dim);
  	gsl_vector_set(x, 0, -10);
	const gsl_multiroot_fsolver_type *T;
  	T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *s;
  	s = gsl_multiroot_fsolver_alloc (T, dim);
  	gsl_multiroot_fsolver_set (s, &function,x);
	
	int status;
  	int iter = 0;
  	do{
    	iter++;
        status = gsl_multiroot_fsolver_iterate (s);
        if (status)break;
        status = gsl_multiroot_test_residual (s->f, 1e-3);
    	}
  	while (status == GSL_CONTINUE && iter < 100);
	printf("Problem 2: \n");
	printf ("iter = %3u minimum at e = %g \n",
        	iter,
        	gsl_vector_get (s->x, 0));
	for(double r=0;r<10;r+=0.1){
		fprintf(stderr,"%g\t%g\n",r,Fe(gsl_vector_get(s->x,0),r));
	}
  	gsl_multiroot_fsolver_free (s);
  	gsl_vector_free (x);
  	return 0;
}


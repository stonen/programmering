#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

struct rparams{double a;double b;};

int rosenbrock(const gsl_vector * x, void *params, gsl_vector * f){
	double a = ((struct rparams *) params)->a;
	double b = ((struct rparams *) params)->b;
	const double x0 = gsl_vector_get (x, 0);
	const double x1 = gsl_vector_get (x, 1); 
  	const double y0 = a * (1 - x0);
  	const double y1 = b * (x1 - x0 * x0);
  	gsl_vector_set (f, 0, y0);
  	gsl_vector_set (f, 1, y1);
	return GSL_SUCCESS;
}

int main (){
  	const gsl_multiroot_fsolver_type *T;
  	gsl_multiroot_fsolver *s;
  	
  	const int dim = 2;
  	struct rparams p;
	p.a = 1;
	p.b = 100;
  	gsl_multiroot_function R;
	R.f = rosenbrock;
	R.n = dim;
	R.params = (void*)&p;
  	gsl_vector *x = gsl_vector_alloc (dim);
  	gsl_vector_set (x, 0, -30);
  	gsl_vector_set (x, 1, -10);

  	T = gsl_multiroot_fsolver_hybrids;
  	s = gsl_multiroot_fsolver_alloc (T, 2);
  	gsl_multiroot_fsolver_set (s, &R,x);
	int status;
  	int iter = 0;
  	do{
    	iter++;
        status = gsl_multiroot_fsolver_iterate (s);
        if (status)break;
        status = gsl_multiroot_test_residual (s->f, 1e-7);
    	}
  	while (status == GSL_CONTINUE && iter < 100);
	printf("Problem 1: \n");	
	printf ("iter = %3u minimum at x = %g and y = %g \n",
        	iter,
        	gsl_vector_get (s->x, 0), 
       		gsl_vector_get (s->x, 1));
  	gsl_multiroot_fsolver_free (s);
  	gsl_vector_free (x);
  	return 0;
}


#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include <assert.h>
#include <gsl/gsl_errno.h>

int ode_hydrogen(double r, const double y[], double dydt[], void * params){
	double e = *(double *)params;
	dydt[0]=y[1];
	dydt[1]=2*(-1/r-e)*y[0];
	return GSL_SUCCESS;
}

double Fe(double e,double r) {
	assert(r>=0);
	const double rmin =1e-3;
	if(r<rmin) return r-r*r;
	gsl_odeiv2_system sys;
	sys.function = ode_hydrogen;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *)&e;
	double acc=1e-6,eps=1e-6,hstart=1e-3;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
	double t=rmin,y[]={t-t*t,1-2*t};
	gsl_odeiv2_driver_apply(driver, &t, r, y);
	return y[0];
}


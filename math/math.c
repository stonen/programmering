#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <tgmath.h>
int main() {
	double  n = 5;
	double  gn = tgamma(5);
	printf("Gamma(%g) = %g \n",n,gn);
	double j = 0.5;
	double jb = j0(j);
	printf("J1(%g) =  %g \n",j,jb);
	double t = 1.0/2.0 ;
	printf("1/2 = %g\n",t);
	double zr =creal (sqrt(2)*I);
	double zi = cimag(sqrt(2)*I);
	printf("Real part of  sqrt(-2) = %g Imag part of sqrt(-2) = %g \n",zr,zi);
	double kr = creal(pow(M_E,I));
	double ki = cimag(pow(M_E,I));
	printf("e^i   r = %g   i =  %g \n",kr,ki);
	double pr = creal(pow(M_E,I*M_PI));
	double pi = cimag(pow(M_E,I*M_PI));
	printf("e^pi*i     r = %g   i = %g \n",pr,pi);
	double yr = creal(cpow(I,M_E));
	double yi = cimag(cpow(I,M_E));
	printf("i^e   r = %g   i = %g  \n",yr,yi);
	float fl = 0.11111111111111111111111111111;
	double db = 0.11111111111111111111111111111;
	long double ldb = 0.11111111111111111111111111111L;
	printf("\nFloating = %.25g \ndouble = %.25lg \nlong double = %.25Lg\n",fl,db,ldb);
	return 0;

}

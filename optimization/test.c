#define ALG gsl_multimin_fminimizer_nmsimplex2
#include<gsl/gsl_multimin.h>
double function_to_minimize (const gsl_vector *g, void *params) {
	double  x = gsl_vector_get(g,0);
	double  y = gsl_vector_get(g,1);
	double result = pow((1-x),2)+100*pow((y-pow(x,2)),2);
	return result;
}

int main(){
	size_t dim = 2;
	gsl_multimin_function F;
	F.f=function_to_minimize;
	F.n=dim;
	F.params=NULL;

	gsl_multimin_fminimizer * state =
	gsl_multimin_fminimizer_alloc (ALG,dim);
	gsl_vector *start = gsl_vector_alloc(dim);
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,1.5); /*x start */
	gsl_vector_set(start,1,1.5); /* ystart */	
	gsl_vector_set_all(step,0.05);
	gsl_multimin_fminimizer_set (state, &F, start, step);

	int iter=0,status;
	double acc=0.00001;
	do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	
	}while(status == GSL_CONTINUE && iter < 99);
	
	double x = gsl_vector_get(state->x,0);
	double y = gsl_vector_get(state->x,1);
	printf("Minimum of rosenbrock function when x = %g and y = %g\n",x,y);
	printf("The minimum should be (1,1) in our case \n");
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	return 0;
}

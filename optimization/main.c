#define ALG gsl_multimin_fminimizer_nmsimplex2
#include<gsl/gsl_multimin.h>
struct experimental_data {int n; double *t,*y,*e;};

double function_to_minimize (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int n=p->n;
	double *t=p->t;
	double *y=p->y;
	double *e=p->e;
	double sum=0;
	for(int i=0;i<n;i++) sum+=pow((A*exp(-(t[i])/T)+B-y[i])/e[i],2);
	return sum;
}

int main(){
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	struct experimental_data params;
	params.n = n;
	params.t = t;
	params.y = y;
	params.e = e;
	
	gsl_multimin_function F;
	F.f=function_to_minimize;
	F.n=3;
	F.params=(void*)&params;

	gsl_multimin_fminimizer * state =
	gsl_multimin_fminimizer_alloc (ALG,F.n);
	gsl_vector *start = gsl_vector_alloc(F.n);
	gsl_vector *step = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,1.5); /*A start */
	gsl_vector_set(start,1,1.5); /* T start */
	gsl_vector_set(start,2,1.5); /* B start */	
	gsl_vector_set_all(step,0.05);
	gsl_multimin_fminimizer_set (state, &F, start, step);

	int iter=0,status;
	double acc=0.01;
	do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	
	}while(status == GSL_CONTINUE && iter < 99);
	
	double A = gsl_vector_get(state->x,0);
	double T = gsl_vector_get(state->x,1);
	double B = gsl_vector_get(state->x,2);
	
	double dt=(t[n-1]-t[0])/50;
	for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt) printf("%g %g\n",ti,A*exp(-ti/T)+B);
	for(int i=0;i<n;i++)fprintf(stderr,"%g %g %g\n",t[i],y[i],e[i]);
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	return 0;
}

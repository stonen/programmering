#include <math.h>
#include <stdio.h>
#include <float.h>
#include "komplex.h"
int main(){
	komplex a = {3,5} ;
	komplex b = {1,3} ;
	komplex z;
	komplex* p = &z;
	komplex r = komplex_sub(a,b);
	komplex add = komplex_add(a,b);
	komplex new = komplex_new(4,7);
	komplex conj = komplex_conjugate(a);
	komplex_set(p,a.re,a.im);
	komplex_print("Her printes a som er sat til {3,5}: ",a);
	komplex_print("Her printes b som er sat til {3,5}: ",b);
	komplex_print("Her printes z som er sat til at være det samme som a: ",z);
	komplex_print("a+b testes = ",add);
	komplex_print("a-b testes = ",r);
	komplex_print("Vi prøver at lave en ny {4,7} = ",new);
	komplex_print("conjugate af a = ",conj);
	if(komplex_equal(a,a, 1e-6,1e-6)){
		printf("Funktionen komplex equal virker hvis dette printes\n");
	}
	else{
		printf("Funktionen komplex equal virker ikke hvis dette printes\n");
	}
	komplex_print("Her printes a*a = ",komplex_mul(a,a));
}

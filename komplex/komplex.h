
typedef struct {double re,im;} komplex;
void komplex_set(komplex* z, double x, double y);
komplex komplex_new(double x,double y);
komplex komplex_add(komplex a, komplex b);
komplex komplex_sub(komplex a, komplex b);
komplex komplex_conjugate(komplex z);
void komplex_print(char *s,komplex z);
int equal(double a, double b, double tau, double epsilon);
int komplex_equal(komplex a, komplex b, double tau, double eps);
komplex komplex_mul(komplex a, komplex b);

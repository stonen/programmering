#include <math.h>
#include <stdio.h>
#include <float.h>
#include "komplex.h"

void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;	
}

komplex komplex_new(double x,double y){
	komplex result = {.re = x, .im = y};
	return result;
}

komplex komplex_add(komplex a, komplex b){
	double x = a.re + b.re;
	double y = a.im + b.im;
	komplex result = {.re = x, .im =y };
	return result;
}

komplex komplex_sub(komplex a, komplex b){
	double x = a.re - b.re;
	double y = a.im - b.im;
	komplex result = {.re = x, .im =y };
	return result;
}

komplex komplex_conjugate(komplex z){
	double x = z.re;
	double y = -z.im;
	komplex result = {.re = x, .im =y};
	return result;
}

void komplex_print(char *s,komplex z){
	printf("%s {%g,%g}\n",s,z.re,z.im);
}

int equal(double a, double b, double tau, double epsilon) {

	if(fabs(a-b)<tau){
		return 1;		
	}
	else if(fabs(a-b)/(fabs(a)+fabs(b))<epsilon/2){
		return 1;
	}
	else{
		return 0;
	}
}

int komplex_equal(komplex a, komplex b, double tau, double eps){
	return equal(a.re,b.re,tau,eps);
}

komplex komplex_mul(komplex a, komplex b){
	double x = a.re*b.re-a.im*b.im;
	double y = a.re*b.im+a.im*b.re;
	komplex result = {.re = x, .im =y };
	return result;
}




#include <gsl/gsl_linalg.h>

int main(){
	double a_data[] = {6.13, -2.90, 5.86,
	     		   8.08, -6.31, -3.89,
	      		  -4.36, 1.00, 0.19};
	double b_data[] = {6.23, 5.37, 2.29};
	gsl_matrix_view a = gsl_matrix_view_array(a_data,3,3);
	gsl_vector_view b = gsl_vector_view_array(b_data,3);
	gsl_vector *x = gsl_vector_alloc(3);
	int t;
	gsl_permutation *p = gsl_permutation_alloc(3);
	gsl_linalg_LU_decomp(&a.matrix, p, &t);
	gsl_linalg_LU_solve(&a.matrix, p, &b.vector, x);
	printf("Her er løsningen til det lineære ligningssystem:\n");
	printf("(x_0 , x1, x2) = \n");
	for(int i=0;i<3;i++)printf("%g\n",gsl_vector_get(x,i));
}

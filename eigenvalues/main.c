#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>

const double pi =3.14159;

int main() {


	int n = 20;
	double s = 1.0/(n+1);
	gsl_matrix *H = gsl_matrix_calloc(n,n);
	for(int i=0; i<n-1; i++){
		gsl_matrix_set(H,i,i,-2);
		gsl_matrix_set(H,i,i+1,1);
		gsl_matrix_set(H,i+1,i,1);
	}
	gsl_matrix_set(H,n-1,n-1,-2);
	gsl_matrix_scale(H,-1/s/s);

	gsl_eigen_symmv_workspace* w =  gsl_eigen_symmv_alloc (n);
	gsl_vector *eval = gsl_vector_alloc(n);
	gsl_matrix *evec = gsl_matrix_calloc(n,n);
	gsl_eigen_symmv(H,eval,evec,w);
	
	gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC);

	fprintf (stderr, "n   exact   calculated\n");
	for (int k=0; k < 9; k++){
    		double exact = pi*pi*(k+1)*(k+1);
    		double calculated = gsl_vector_get(eval,k);
    		fprintf (stderr, "%i   %g   %g\n", k+1, exact, calculated);
	}
		printf("x \tu0 \tu1 \tu2 \n");
 		printf("%g %g %g %g\n",0.0,0.0,0.0,0.0);
		for(int i=0;i<n;i++) printf("%g %g %g %g\n",(i+1.0)/(n+1),\
		gsl_matrix_get(evec,i,0),gsl_matrix_get(evec,i,1),gsl_matrix_get(evec,i,2));
  		printf("%g %g %g %g\n",1.0,0.0,0.0,0.0);
  		printf("\n\n");
  	
	return 0;
}	



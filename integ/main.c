#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

double gauss_integrand(double x, void* params){
	double z = *(double*)params;
	return exp(-z*pow(x,2));
}

double norm_integral(double z){
	gsl_function f;
	f.function = gauss_integrand;
	f.params = (void*)&z;
	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
int status=gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}

double hamil_integrand(double x, void* params){
	double t = *(double*)params;
	return (-pow(t,2)*pow(x,2)/2+t/2+pow(x,2)/2)*exp(-t*pow(x,2));
}

double hamilton_integral(double t){
	gsl_function h;
	h.function = hamil_integrand;
	h.params = (void*)&t;
	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
int status=gsl_integration_qagi(&h,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}

int main(){
	double a=0.1, b=4, dx=0.01;
	printf("x \talfa \n");
	for(double x=a;x<b;x+=dx)
		printf("%g %g\n",x,hamilton_integral(x)/norm_integral(x));
return 0;
}

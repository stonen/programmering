#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

double integrand(double x, void* params){
	return log(x)/sqrt(x);
}

double integration_result(){
	gsl_function f;
	f.function = integrand;
	f.params = NULL;
	int limit = 100;
	double a=0,b=1,acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
int status=gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}

int main(){
	printf("Result of integral: %g \n",integration_result());
	return 0;
}

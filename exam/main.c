#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int sqrt_ode(double t, const double y[], double dydt[], void * params){
	dydt[0]=1/(2*y[0]);	
	return GSL_SUCCESS;
}

double mysqrt(double x){
	if(x==0) return 0;
	if(x<1) return 1/mysqrt(1/x);
	if(x>4) return 2*mysqrt(x/4);
	gsl_odeiv2_system sys;
	sys.function = sqrt_ode;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=1,y[]={1};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	double a=0,b=10,dx=0.2;
	printf("x\tmysqrt\tmathsqrt\n");
	for(double x=a;x<b+dx;x+=dx)printf("%g %g %g\n",x,mysqrt(x),sqrt(x));
	return 0;
}	

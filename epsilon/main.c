#include <stdio.h>
#include "maxmin.h"
#include <limits.h>
#include <float.h>
#include "meps.h"
#include "sum.h"
#include "epsilon.h"

int main() {
	
	printf("Max integer fra imits.h: %i \n",INT_MAX);
	printf("Max integer ved while loop: %i \n",maxw());
	printf("Max integer ved do-while loop: %i \n",maxdw());
	printf("Max integer ved for loop: %i \n",maxf());
	printf("Min integer fra imits.h: %i \n",INT_MIN);
	printf("Min integer ved while loop: %i \n",minw());
	printf("Min integer ved do-while loop: %i \n",mindw());
	printf("Min integer ved for loop: %i \n",minf());
	printf("Machine epsilon fra float.h (float): %g \n",FLT_EPSILON);
	printf("Machine epsilon fra float.h (DOUBLE): %g \n",DBL_EPSILON);
	printf("Machine epsilon fra float.h (LONG DOUBLE): %Lg \n",LDBL_EPSILON);
	printf("Machine epsilon while loop: %g \n",epsw());
	printf("Machine epsilon do while loop: %g \n",epsdw());
	printf("Machine epsilon for loop: %g \n",epsf());
	int max = INT_MAX/3;
	printf("Sum_up_float: %g \n",suf(max));
	printf("Sum_down_float: %g \n",sdf(max));
	printf("Sum_up_double: %g \n",sud(max));
	printf("Sum_down_double: %g \n",sdd(max));
	if(equal(5.0,5.0,0.0001,0.0001)){
		printf("Det virker\n");	
	}
	else {
		printf("Det virker ikke\n");
	}
	
	return 0;
}



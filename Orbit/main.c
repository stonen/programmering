#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int orbit_dif
(double t, const double y[], double dydt[], void * params)
{
	double epsilon = *(double *) params;
	dydt[0]=y[1];
	dydt[1]=1-y[0]+epsilon*y[0]*y[0];

	
	return GSL_SUCCESS;
}


int main (int argc, char *argv[]) {
	double epsilon = atof(argv[2]);
	double uprime = atof(argv[1]);


	gsl_odeiv2_system sys;
	sys.function = orbit_dif;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &epsilon;

	double acc=1e-6,eps=1e-6,hstart=1e-3;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[2]={1, uprime};
	
	
	double a=0,b=100,dx=0.01;
	
	for(double x=a;x<b+dx;x+=dx){
		gsl_odeiv2_driver_apply(driver, &t, x, y);
		printf("%g %g\n",x,y[0]);
	}
	return 0;
}

	

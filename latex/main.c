#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int errordiff
(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=M_2_SQRTPI*exp(-pow(t,2));
	return GSL_SUCCESS;
}


double error(double x){
	gsl_odeiv2_system sys;
	sys.function = errordiff;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;
	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
	(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
	double t=0,y[1]={0};
	gsl_odeiv2_driver_apply(driver, &t, x, y);
	return y[0];
}

int main (int argc, char *argv[]) {
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	for(double x=a;x<b+dx;x+=dx){
	printf("%g  \t%g \n",x,error(x));
	}
	return 0;
}
